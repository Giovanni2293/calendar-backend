package com.example.calendarBackend.event.application;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.calendarBackend.event.domain.Event;
import com.example.calendarBackend.event.infraestructure.EventSearcher;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class EventRestController {

	@Autowired
	private EventSearcher eventSearcher;

	@GetMapping("/events")
	public ResponseEntity<List<Event>> index() {
		List<Event> events =  eventSearcher.findAll();
		if(!events.isEmpty()) return new ResponseEntity<>(events, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping(value = "/events/{id}")
	public ResponseEntity<Optional<Event>> getEventById(@PathVariable Long id) {
		Optional<Event> event = eventSearcher.findById(id);
		if(!event.isEmpty()) return new ResponseEntity<>(event, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping(value = "/events/{id}")
	public ResponseEntity<?> deleteEventbyId(@PathVariable Long id) {
		if(eventSearcher.deleteById(id)) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/events")
	public ResponseEntity<Event> create(@RequestBody Event event) {
		if(eventSearcher.save(event) != null) return new ResponseEntity<>(event, HttpStatus.CREATED);
		return new ResponseEntity<>(event, HttpStatus.CONFLICT);
	}
	
	@PutMapping(value = "/events/{id}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<Event> update(@RequestBody Event event, @PathVariable Long id) {
		Optional<Event> optionalEvent = eventSearcher.findById(id);
		if (!optionalEvent.isEmpty()) {
			Event currentEvent = optionalEvent.get();
			currentEvent.setEventTitle(event.getEventTitle());
			currentEvent.setEventDescription(event.getEventDescription());
			currentEvent.setEventPrimaryColor(event.getEventPrimaryColor());
			currentEvent.setEventSecondaryColor(event.getEventSecondaryColor());
			currentEvent.setStartDate(event.getStartDate());
			currentEvent.setEndDate(event.getEndDate());
			return new ResponseEntity<>(eventSearcher.save(currentEvent), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
