package com.example.calendarBackend.event.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="events")
public class Event implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4205390080646303285L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long eventId;
	@Column(length = 50)
	private String eventTitle;
	@Column(length = 250)
	private String eventDescription;
	@Column(length = 7)
	private String eventPrimaryColor;
	@Column(length = 7)
	private String eventSecondaryColor;
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	public Long getEventId() {
		return eventId;
	}
	
	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public String getEventPrimaryColor() {
		return eventPrimaryColor;
	}

	public void setEventPrimaryColor(String eventPrimaryColor) {
		this.eventPrimaryColor = eventPrimaryColor;
	}

	public String getEventSecondaryColor() {
		return eventSecondaryColor;
	}

	public void setEventSecondaryColor(String eventSecondaryColor) {
		this.eventSecondaryColor = eventSecondaryColor;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
