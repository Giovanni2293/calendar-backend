package com.example.calendarBackend.event.domain.ports;

import java.util.List;
import java.util.Optional;

import com.example.calendarBackend.event.domain.Event;

public interface IEventApiPort {

	public List<Event> findAll();
	public Optional<Event> findById(Long id);
	public boolean deleteById(Long id);
	public Event save(Event event);
}
