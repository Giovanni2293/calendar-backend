package com.example.calendarBackend.event.domain.ports;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.calendarBackend.event.domain.Event;

public interface IEventPersistencyPort extends JpaRepository<Event, Long> {

}
