package com.example.calendarBackend.event.infraestructure;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.calendarBackend.event.domain.Event;
import com.example.calendarBackend.event.domain.ports.IEventApiPort;
import com.example.calendarBackend.event.domain.ports.IEventPersistencyPort;

@Service
public class EventSearcher implements IEventApiPort {

	@Autowired
	private IEventPersistencyPort eventRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Event> findAll() {
		return eventRepository.findAll();
	}

	@Override
	@Transactional
	public Optional<Event> findById(Long id) {
		return eventRepository.findById(id);
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {
		if (!findById(id).isEmpty()) {
			eventRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	@Transactional
	public Event save(Event event) {
		return eventRepository.save(event);
	}
}
