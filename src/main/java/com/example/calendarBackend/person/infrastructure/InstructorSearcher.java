package com.example.calendarBackend.person.infrastructure;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.calendarBackend.person.domain.Instructor;
import com.example.calendarBackend.person.domain.ports.IInstructorApiPort;
import com.example.calendarBackend.person.domain.ports.IInstructorPersistencyPort;

@Service
public class InstructorSearcher implements IInstructorApiPort {

	@Autowired
	private IInstructorPersistencyPort instructorRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Instructor> findAll() {
		return (List<Instructor>) instructorRepository.findAll();
	}

	@Override
	public Optional<Instructor> findByDocumentNumber(Long documentNumber) {
		return instructorRepository.findByDocumentNumber(documentNumber);
	}
	
	

}
