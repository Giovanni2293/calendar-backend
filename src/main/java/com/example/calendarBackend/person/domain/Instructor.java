package com.example.calendarBackend.person.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "instructors")
public class Instructor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4151859684019629166L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long instructorId;
	@Column(length = 3)
	private String documentType;
	@Column(length = 20)
	private Long documentNumber;
	private String firstName;
	private String lastName;
	@Temporal(TemporalType.DATE)
	private Date birthday;

	public Instructor() {
		super();
	}

	public Instructor(String documentType, Long documentNumber, String firstName, String lastName, Date birthday) {
		super();
		this.documentType = documentType;
		this.documentNumber = documentNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
	}

	public Long getInstructorId() {
		return instructorId;
	}

	public void setInstructorId(Long instructorId) {
		this.instructorId = instructorId;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public Long getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(Long documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
}
