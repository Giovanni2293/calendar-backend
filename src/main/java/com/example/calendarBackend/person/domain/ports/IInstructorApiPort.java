package com.example.calendarBackend.person.domain.ports;

import java.util.List;
import java.util.Optional;

import com.example.calendarBackend.person.domain.Instructor;

public interface IInstructorApiPort {
	
	public List<Instructor> findAll();
	public Optional<Instructor> findByDocumentNumber(Long documentNumber);
	
}
