package com.example.calendarBackend.person.domain.ports;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.calendarBackend.person.domain.Instructor;

public interface IInstructorPersistencyPort extends JpaRepository<Instructor, Long> {
	public Optional<Instructor> findByDocumentNumber(Long documentNumber);
}
