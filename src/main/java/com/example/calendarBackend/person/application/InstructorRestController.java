package com.example.calendarBackend.person.application;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.calendarBackend.person.domain.Instructor;
import com.example.calendarBackend.person.infrastructure.InstructorSearcher;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class InstructorRestController {

	@Autowired
	private InstructorSearcher instructorSearcher;

	@GetMapping("/instructors")
	public ResponseEntity<List<Instructor>> index() {
		List<Instructor> instructors = instructorSearcher.findAll();
		if (!instructors.isEmpty()) return new ResponseEntity<>(instructors, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/instructors/{documentNumber}")
	public ResponseEntity<Optional<Instructor>> getInstructorById(@PathVariable Long documentNumber) {
		Optional<Instructor> optionalInstructor = instructorSearcher.findByDocumentNumber(documentNumber);
		if (!optionalInstructor.isEmpty()) return new ResponseEntity<>(optionalInstructor, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
