INSERT INTO instructors (document_type, document_number, birthday, first_name, last_name) VALUES ('CC', '111111', '2000-01-01', 'Alexis', 'Dias');
INSERT INTO instructors (document_type, document_number, birthday, first_name, last_name) VALUES ('CC', '222222', '2000-02-02', 'Carlos', 'Pérez');
INSERT INTO instructors (document_type, document_number, birthday, first_name, last_name) VALUES ('CC', '333333', '2000-03-03', 'Lucho', 'Serrano');
INSERT INTO instructors (document_type, document_number, birthday, first_name, last_name) VALUES ('CC', '444444', '2000-04-04', 'Leo', 'Acosta');
INSERT INTO instructors (document_type, document_number, birthday, first_name, last_name) VALUES ('CC', '555555', '2000-05-05', 'Miguel', 'Torres');
INSERT INTO events  (event_title, event_description, event_primary_color, event_secondary_color, start_date, end_date) VALUES ('A week off', 'test', '#AD2121', '#FAE3E3', '2021-07-24 00:00', '2021-07-30 16:09');
INSERT INTO events (event_title, event_description, event_primary_color, event_secondary_color, start_date, end_date) VALUES ('Seminar A', 'test', '#E3BC08', '#FDF1BA', '2021-07-31 00:00', '2021-08-06 16:09');
INSERT INTO events  (event_title, event_description, event_primary_color, event_secondary_color, start_date, end_date) VALUES ('Seminar B', 'test', '#E3BC08', '#FDF1BA', '2021-08-07 00:00', '2021-08-13 16:09');
INSERT INTO events  (event_title, event_description, event_primary_color, event_secondary_color, start_date, end_date) VALUES ('Proyect week', 'test', '#1E90FF', '#D1E8FF', '2021-08-14 00:00', '2021-08-20 16:09');