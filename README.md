## Steps to setup

### Previous requeriments
1. Java Oracle JDK 11.
2. MYSQL server with workbench(Optional visual administrator)
3. Create a database schema called `db_calendar_backend`

The next steps are optional, if you don't want to create a new user for this test,
use another of your preference but be sure that the username and password match and
that user has all access granted to the schema db_calendar_backend.

4. Create a username called `learning`
5. Give the user the password `1234`
6. Grant that user all permisions to the schema with the following command.
`GRANT ALL PRIVILEGES ON db_calendar_backend.* TO 'learning'@'localhost';`

All data that is used for testing, is auto generated using the file located in 
`src/main/resources/import.sql`

### IDE download

You will have two options to do this step.

#### Option 1) Download official IDE from Spring

1. Select Spring tools 4 for Eclipse https://spring.io/tools and then select 
your operative system.
2. Extract the folder downloaded.
3. Create a folder that will containt the workspace.
4. Enter into the folder extracted and then execute Eclipse.
5. Select your workspace created in step 3.

#### Option 2) Download Eclipse IDE

The steps for this option are the same as the Download official IDE from Spring
except step one that is from this url: https://www.eclipse.org/downloads/packages/release/2021-06/r/eclipse-ide-enterprise-java-and-web-developers.

1. Once you completed the 5 steps before, inside Eclipse select Help -> Eclipse Marketplace.
2. Write in the search bar Spring tools 4 and hit install, accept all terms and after that, it will ask
you to restart.
3. After you restart Eclipse IDE click in the option located in Window -> show view -> other -> Boot dashboard

#### Clonning project and adding to Eclipse/Spring tools

Once you did the previous steps do the following: 
1. select New -> Import -> Projects from git (with smart import)
2. Click Next -> clone URI.
3. Click Next -> then paste this uri into field uri: https://gitlab.com/Giovanni2293/calendar-backend.git 
3. Click Next -> Leave only master checked (click other branches for un-check)
4. Click Next -> Browse your directory (workspace created in step 3) 
5. Click the button Save twice
6. Finish

After those steps. Dependencies will start to download (Project is created with maven).
Spring boot has an embeded Apache server. To start it and deploy the services created you have to:

1. In Boot Dashboard, click on the arrow in local.
2. It will show the name calendar-backend[devtools]
3. Hit right click on calendar-backend[devtools]
4. Select (re)start

This will start the server and after that you can start testing the services with any tool.

The base url is http://localhost:8080

Example: http://localhost:8080/api/events
 
